package jpa.relationships;

import ch.qos.logback.core.net.SyslogOutputStream;
import jpa.relationships.entity.Address;
import jpa.relationships.entity.Project;
import jpa.relationships.entity.School;
import jpa.relationships.entity.Student;
import jpa.relationships.repository.ProjectRepository;
import jpa.relationships.repository.StudentRepository;
import jpa.relationships.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Optional;

@SpringBootApplication
@RequiredArgsConstructor
public class RelationshipsApplication {

	private final StudentRepository studentRepository;
	private final StudentService studentService;
	private final ProjectRepository projectRepository;

	public static void main(String[] args) {
		SpringApplication.run(RelationshipsApplication.class, args);
	}

	@Bean
	public CommandLineRunner run() {
		return args -> {

			studentService.createData();
			studentService.listData();

//			Project project = Project.builder()
//					.project_name("JPA Relationships").build();
//
//			Long projectId = projectRepository.save(project).getId();
//
//			projectRepository.findById(projectId).ifPresent(System.out::println);

		};
	}



}
