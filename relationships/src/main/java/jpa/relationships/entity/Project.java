package jpa.relationships.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
//@Getter
//@Setter
@EqualsAndHashCode(callSuper = false, exclude = {"students"})
@ToString(exclude = "students")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "projects")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String project_name;

    @ManyToMany(mappedBy = "projects", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set <Student> students;
}
