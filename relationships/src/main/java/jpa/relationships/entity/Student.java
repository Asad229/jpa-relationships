package jpa.relationships.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
//@Data
@EqualsAndHashCode(callSuper = false, exclude = {"projects"})
@Setter
@Getter
@ToString(exclude = "projects")
@Table(name = "student")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    private int age;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_address")
    private Address address;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "school_id", referencedColumnName = "id")
    private School school;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "students_projects",
        joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "project_id", referencedColumnName = "id"))
    private Set<Project> projects;

}
