package jpa.relationships.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "school")
@ToString(exclude = "students")
public class School {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nameOfSchool;

    @OneToMany(
            mappedBy = "school",
            cascade = CascadeType.PERSIST,
            fetch = FetchType.EAGER
    )
    private List<Student> students;


}
