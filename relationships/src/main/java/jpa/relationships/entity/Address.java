package jpa.relationships.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.*;

@Slf4j
@Entity
//@Data
@Setter
@Getter
@Table(name = Address.TABLE_NAME)
@ToString(exclude = "student")
public class Address {

    public static final String TABLE_NAME = "addresses";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String street;
    private String city;

    @OneToOne(mappedBy = "address")
    private Student student;

    @PrePersist
    public void logNewAddressAttempt(){
        log.info("This address is being saved {}", this);
    }

    @PostPersist
    public void logNewAddressAdded(){
        log.info("{} address has been created", this);
    }

    @PreRemove
    public void logAddressRemoveAttempt(){
        log.info("{} address is being removed", this);
    }

    @PostRemove
    public void logAddressRemoved(){
        log.info("{} address has been removed", this);
    }

    @PreUpdate
    public void logAddressUpdateAttempt(){
        log.info("Address is being updated {}", this);
    }

    @PostUpdate
    public void logAddressUpdated(){
        log.info("{} address has been updated", this);
    }

    @PostLoad
    public void logAddressLoad(){
        log.info("Address loaded");
    }
}
