package jpa.relationships.service;


import jpa.relationships.dto.GenericSearchDto;
import jpa.relationships.entity.Address;
import jpa.relationships.entity.Project;
import jpa.relationships.entity.School;
import jpa.relationships.entity.Student;
import jpa.relationships.repository.StudentRepository;
import jpa.relationships.search.SearchCriteria;
import jpa.relationships.search.SearchSpecification;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;
    private final ModelMapper mapper;

    public Student save(Student student){
        return  studentRepository.save(student);
    }



    @Transactional
    public void createData() {

        Student student = new Student();
        student.setName("Asad");
        student.setSurname("Hamidov");
        student.setAge(23);

        Address address = new Address();
        address.setStreet("Stary Rynek Oliwski");
        address.setCity("Gdansk");
        student.setAddress(address);

        School school = new School();
        school.setNameOfSchool("TET Gimnaziya");
        student.setSchool(school);

        Project project = Project.builder()
                .project_name("ManyToMany")
                .build();

        Project project1 = Project.builder()
                .project_name("Microservice")
                .build();

        student.setProjects(Set.of(project, project1));

        studentRepository.save(student);

        Student student1 = new Student();
        student1.setName("Ibo");
        student1.setSurname("Hamidov");
        student1.setAge(25);
        student1.setSchool(school);

        student1.setProjects(Set.of(project, project1));

        studentRepository.save(student1);

    }

    @Transactional
    public void listData(){
        studentRepository.findAll().forEach(
                s->{
                    System.out.println(s);
                    s.getProjects().forEach(i-> System.out.println(i.getProject_name()));
                }
        );
    }

    public List<Student> findStudentBySpecification() {
        studentRepository.findAll((root, query, cb) -> cb.equal(root.get("firstName"), "Asad"))
                .forEach(s -> System.out.println("Student:" + s));
        return null;
    }


    @Transactional
    public Page<Student> search(GenericSearchDto filter, Pageable pageable) {
        return studentRepository.findAll(new SearchSpecification<>(filter.getCriteria()), pageable)
                .map(p -> mapper.map(p, Student.class));
    }
}
