package jpa.relationships.controller;

import jpa.relationships.dto.GenericSearchDto;
import jpa.relationships.entity.Student;
import jpa.relationships.search.SearchCriteria;
import jpa.relationships.service.StudentService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
public class StudentController {

    private final StudentService studentService;

    @PostMapping("/student/save")
    public Student save (@RequestBody Student student){
        var response = studentService.save(student);
        log.info("Student {} is added", student.getName());
        return response;
    }

    @PostMapping("/student/search")
    public Page<Student> searchStudent(@RequestBody GenericSearchDto search, Pageable pageable){
        return studentService.search(search, pageable);
    }

}
